﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerInput : MonoBehaviour
{
    public float Factor = 7f;
    public float JumpFactor = 30f;
    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
            _rigidbody.AddForce(Vector3.forward * Factor);
        if (Input.GetKey(KeyCode.A))
            _rigidbody.AddForce(Vector3.left * Factor);
        if (Input.GetKey(KeyCode.S))
            _rigidbody.AddForce(Vector3.back * Factor);
        if (Input.GetKey(KeyCode.D))
            _rigidbody.AddForce(Vector3.right * Factor);
        if (Input.GetKey(KeyCode.Space))
            _rigidbody.AddForce(Vector3.up * JumpFactor);
    }
}
