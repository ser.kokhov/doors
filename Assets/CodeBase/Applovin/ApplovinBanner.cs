﻿using UnityEngine;

namespace CodeBase
{
    public class ApplovinBanner : MonoBehaviour
    {
        [SerializeField] private Color _backgroundColor;
        [SerializeField] private GA _gameAnalitics;
        [SerializeField] private FirebaseInit _firebaseInit;

        private string _adUnitId;

        public void Initialize(string adUnitId)
        {
            _adUnitId = adUnitId;

            // Banners are automatically sized to 320×50 on phones and 728×90 on tablets
            // You may call the utility method MaxSdkUtils.isTablet() to help with view sizing adjustments
            MaxSdk.CreateBanner(_adUnitId, MaxSdkBase.BannerPosition.BottomCenter);

            // Set background or background color for banners to be fully functional
            MaxSdk.SetBannerBackgroundColor(_adUnitId, _backgroundColor);
        }

        public void Show()
        {
            MaxSdk.ShowBanner(_adUnitId);
            _gameAnalitics.BannerStart();
            _firebaseInit.BannerStart();
        }

        public void Hide()
        {
            MaxSdk.HideBanner(_adUnitId);
            _gameAnalitics.BannerClosed();
            _firebaseInit.BannerClosed();
        }
    }
}