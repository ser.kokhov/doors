using System;
using UnityEngine;

namespace CodeBase
{
    public interface IMonetization
    {
        void Initialize();
        void ShowInterstitial(string placement, Action callback);
        void ShowRewarded(string placement, Action<bool> callback);
        void ShowBanner();
    }

    public class ApplovinToyStream : MonoBehaviour, IMonetization
    {
        [SerializeField] private ApplovinInterstitial _interstitial;
        [SerializeField] private ApplovinRewarded _rewarded;
        [SerializeField] private ApplovinBanner _banner;

        private const string InterstitialId = "9c50588b93394f2e";
        private const string RewardedId = "2588f7858dac35e6";
        private const string BannerId = "86e321222bbfa6cf";

        public string SdkName => "Applovin";

        public void Initialize()
        {
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                _interstitial.Initialize(InterstitialId);
                //_rewarded.Initialize(RewardedId);
                _banner.Initialize(BannerId);
                ShowBanner();
            };

            MaxSdk.SetSdkKey("CvFQ6rVnxKktdZBfCvb-FTbPJcBiFOV7Fp94Oau3NX4O5ywf5948NLCrRGKdKSH9vg2zI_AyEuxLTP9-wIZZDn");
            MaxSdk.InitializeSdk();
        }

        public void ShowInterstitial(string placement, Action callback) => _interstitial.Show(placement, callback);
        public void ShowRewarded(string placement, Action<bool> callback) => _rewarded.Show(placement, callback);
        public void ShowBanner() => _banner.Show();
    }
}
