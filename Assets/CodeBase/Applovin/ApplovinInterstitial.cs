﻿using UnityEngine;
using System;

namespace CodeBase
{
    public class ApplovinInterstitial : MonoBehaviour
    {
        [SerializeField] private GA _gameAnalitics;
        [SerializeField] private FirebaseInit _firebaseInit;

        private string _adUnitId;
        private int _retryAttempt;
        private Action _callback;

        public void Initialize(string adUnitId)
        {
            _adUnitId = adUnitId;
            
            MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
            MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialLoadFailedEvent;
            MaxSdkCallbacks.Interstitial.OnAdDisplayedEvent += OnInterstitialDisplayedEvent;
            MaxSdkCallbacks.Interstitial.OnAdClickedEvent += OnInterstitialClickedEvent;
            MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialHiddenEvent;
            MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += OnInterstitialAdFailedToDisplayEvent;
            
            LoadInterstitial();
        }

        public void Show(string placement, Action callback)
        {
            _callback = callback;

            if (MaxSdk.IsInterstitialReady(_adUnitId))
            {
                MaxSdk.ShowInterstitial(_adUnitId, placement);                
                _gameAnalitics.InterShown();
                _firebaseInit.InterShown();
            }
        }

        private void LoadInterstitial()
        {
            MaxSdk.LoadInterstitial(_adUnitId);
            _gameAnalitics.InterStatrt();
            _firebaseInit.InterStatrt();
        }

        private void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Interstitial ad is ready for you to show. MaxSdk.IsInterstitialReady(adUnitId) now returns 'true'

            // Reset retry attempt
            _retryAttempt = 0;
        }

        private void OnInterstitialLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
        {
            // Interstitial ad failed to load 
            // AppLovin recommends that you retry with exponentially higher delays, up to a maximum delay (in this case 64 seconds)

            _gameAnalitics.InterFailed();
            _firebaseInit.InterFailed();

            _retryAttempt++;
            double retryDelay = Math.Pow(2, Math.Min(6, _retryAttempt));


            Invoke(nameof(LoadInterstitial), (float)retryDelay);
        }

        private void OnInterstitialDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo) { }

        private void OnInterstitialAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
        {
            // Interstitial ad failed to display. AppLovin recommends that you load the next ad.
            LoadInterstitial();
        }

        private void OnInterstitialClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo) { }

        private void OnInterstitialHiddenEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Interstitial ad is hidden. Pre-load the next ad.
            LoadInterstitial();

            _callback.Invoke();
        }
    }
}