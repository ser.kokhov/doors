using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using System;
using TMPro;

public class DoorOpen : MonoBehaviour
{
    //[SerializeField] private Transform doorTransform;
    [SerializeField] private float duration;
    [SerializeField] public int id;

    [SerializeField] private TMP_Text _idText;

    public Action<DoorOpen> Entered;

    private bool answer;

    void Start()
    {
        GetComponent<BoxCollider>();
    }

    private void Update()
    {
        _idText.text = $"{id}";
    }

    public void OnTriggerEnter()
    {
        GetComponent<BoxCollider>().enabled = false;
        //transform.DORotate(doorTransform.rotation.eulerAngles, duration);
        Entered?.Invoke(this);
        //Debug.Log(id);
    }
}

