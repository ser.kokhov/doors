﻿using UnityEngine ;

public class Player : MonoBehaviour {
   [SerializeField] private float moveSpeed ;
   [SerializeField] private float pushForce ;
   [SerializeField] private float playerPositionX;
   [Space]
   [SerializeField] private TouchSlider touchSlider ;

   private bool isPointerDown ;
   private bool canMove ;
   private Vector3 playerPosition ;

    private Vector3 _distance;
    [SerializeField] private float _gravity;

    [SerializeField] private float _speed;
    private float _maxSpeed = 110; //максимальная скорость игрока

    private void Start () {
      canMove = true ;

      touchSlider.OnPointerDownEvent += OnPointerDown ;
      touchSlider.OnPointerDragEvent += OnPointerDrag ;
      //touchSlider.OnPointerUpEvent += OnPointerUp ;
   }

   private void Update () {
      if (isPointerDown)
         gameObject.transform.position = Vector3.Lerp (gameObject.transform.position, playerPosition, moveSpeed * Time.deltaTime);
     
     UpdatePlayerPosition();
    }

   private void OnPointerDown () {
      isPointerDown = true ;
   }

   private void OnPointerDrag (float xMovement) {
      if (isPointerDown) 
      {
            playerPosition = gameObject.transform.position ;
            playerPosition.x = xMovement * playerPositionX ;
      }
   }

    private void UpdatePlayerPosition()//обновление позиции персонажа
    {
        Vector3 _targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;
        

        Vector3 _dif = _targetPosition - transform.position;
        Vector3 _moveDir = _dif.normalized * 25 * Time.deltaTime;
    }

    void FixedUpdate()
    {
        MovePleyer();
    }

    private void MovePleyer()//передвижеие игрока
    {
        _distance.z = _speed;
        _distance.y += _gravity * Time.fixedDeltaTime;
    }

    private void OnPointerUp () {
      /*if (isPointerDown && canMove) {
         isPointerDown = false ;
         canMove = false ;

            // Push the cube:
            gameObject.CubeRigidbody.AddForce (Vector3.forward * pushForce, ForceMode.Impulse) ;

         Invoke ("SpawnNewCube", 0.3f) ;
      }*/
   }

   private void OnDestroy () {
      //remove listeners:
      touchSlider.OnPointerDownEvent -= OnPointerDown ;
      touchSlider.OnPointerDragEvent -= OnPointerDrag ;
      touchSlider.OnPointerUpEvent -= OnPointerUp ;
   }
}
