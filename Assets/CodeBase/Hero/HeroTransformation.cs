﻿using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class HeroTransformation : MonoBehaviour
{
    [SerializeField] private GameObject[] _weapons;

    [SerializeField] private GameObject[] _confetti;
    [SerializeField] private GameObject _prison;
    [SerializeField] private GameObject _ice;

    [SerializeField] private float _increase;
    [SerializeField] public float _playerForce;

    [SerializeField] private Animator _animator;

    public float _speed = 1;
    public int coinsScore;

    public Action Win; 
    public Action Lose;

    public Action BonusWin;
    public Action<string> BonusLose;

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _playerForce = 1f;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            //_animator.Play("Win");
            //_confetti.SetActive(true);
            Debug.Log("win");
            Win?.Invoke();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            _animator.Play("Win");
            _confetti[0].SetActive(true);
            //Destroy(other.gameObject);
            Win?.Invoke();
            
            other.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        else if (other.gameObject.tag == "Obstacle")
        {
            Lose?.Invoke();
        }
        else if (other.gameObject.tag == "Star")
        {
            _confetti[1].SetActive(true);

            int stars = PlayerPrefs.GetInt("Stars");
            stars++;
            PlayerPrefs.SetInt("Stars", stars);

            Destroy(other.gameObject);
            Debug.Log("star");
        }
        else
        {
            _animator.Play("Flipping");
            _confetti[1].GetComponent<ParticleSystem>().Play();
        }
    }

    public void Run()
    {
        int level = PlayerPrefs.GetInt("Level");

        if (level % 5 == 0 && level != 0)
        {
            _animator.Play("Balance");
        }
        else
        {
            _animator.Play("Running");
        }
    }
}
