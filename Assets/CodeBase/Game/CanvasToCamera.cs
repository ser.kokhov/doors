using UnityEngine;

public class CanvasToCamera : MonoBehaviour
{
    [SerializeField] public Transform _camera;

    void Update()
    {
        transform.forward = _camera.forward;  
    }
}
