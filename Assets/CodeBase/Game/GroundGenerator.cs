using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGenerator : MonoBehaviour
{
    public GameObject[] _groundPrefabs; 
    public GameObject _starPrefabs;
    private List<GameObject> _activeGroundPanel = new List<GameObject>();
    private float _spawnPosition = 0;
    private float _groundLength = 53.8f;//����� ���������
    private int level;

    [SerializeField] private Transform _player;

    [SerializeField] private GameObject Ground;

    private int randomBonus;
    private int _startGround = 6; //��� �������� ��� ������������ 3 ���������

    public void Awake()
    {
        randomBonus = Random.Range(0, 5);
        level = PlayerPrefs.GetInt("Level");
        //_startGround = Random.Range(4, 6);
    }
    private void SpawnNextPanel()//����� ����������� �������� , ���� ������ �������� �� 55%
    {
        if (_player.position.z - 30 > _spawnPosition - (_startGround * _groundLength))
        {
            SpawnGroundPanel(Random.Range(0, _groundPrefabs.Length));//����� ��������� ���������
            DeleteGroundPanel();//�������� ��������� ���������
        }
    }

    private void SpawnGroundPanel(int _groungIndex)//����� ��������� 
    {
        level = PlayerPrefs.GetInt("Level");

        GameObject _nextGround = Instantiate(_groundPrefabs[_groungIndex], transform.forward * _spawnPosition, transform.rotation);
        _nextGround.transform.parent = Ground.transform;
        _activeGroundPanel.Add(_nextGround);//������� ������ �� ������ 
        _spawnPosition += _groundLength;//������� ������ = ����� ������� +����� ���������
    }

    private void SpawnStars()//����� ��������� 
    {
        float x = Random.Range(-3.2f, 3.2f);
        float y = 1;//Random.Range(0f, 100f);
        float z = Random.Range(-5.5f, 277f);
        Vector3 pos = new Vector3(x, y, z);
        GameObject star = Instantiate(_starPrefabs, pos, _starPrefabs.transform.rotation) as GameObject;
        star.transform.parent = Ground.transform;
    }

    private void DeleteGroundPanel()//�������� ���������
    { 
        Destroy(_activeGroundPanel[0]);
        _activeGroundPanel.RemoveAt(0);
    }

    public void ViewStartPanel()//����������� ��������� ��������
    {
        level = PlayerPrefs.GetInt("Level");

        if (level % 5 == 0 && level != 0)
        {
            for (int i = 0; i < _startGround; i++)
            {
                SpawnGroundPanel(randomBonus);
            }
        }

        else
        {
            for (int i = 0; i < _startGround; i++)
            {
                SpawnGroundPanel(Random.Range(6, _groundPrefabs.Length));
            }
        }

        for (int i = 0; i < 3; i++)
        {
            SpawnStars();
        }
    }
}
