using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAnimations : MonoBehaviour
{
    [SerializeField] public Text Text;
    private string text;

    private void Start()
    {
        text = Text.text;
        Text.text = "";
        StartCoroutine(TextRutine());
    }

    IEnumerator TextRutine()
    {
        foreach(char abc in text)
        {
            Text.text += abc;
            yield return new WaitForSeconds(0.02f);
        }
    }

}
