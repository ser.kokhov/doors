using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform endPoint;

    [SerializeField] private Slider slider;

    float maxDistance;

    void Start()
    {
        maxDistance = GetDistance();
    }

    void Update()
    {
        if (player.position.z<=maxDistance && player.position.z>=endPoint.position.z)
        {
            float distance = 1 - (GetDistance() / maxDistance);
            SetProgress(distance);
        }
    }

    float GetDistance()
    {
        return Vector3.Distance(endPoint.position, player.position);
    }

    void SetProgress(float p)
    {
        slider.value = p;
    }
}
