using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

using TMPro;

namespace CodeBase
{
    public class Game : MonoBehaviour
    {
        [Header("Analitics")]
        [SerializeField] private ToyStreamSDK _toyStreamSdk;
        [SerializeField] private GA _gameAnalitics;
        [SerializeField] private FirebaseInit _firebase;
        [SerializeField] private bool _viewAds;

        [Header("Panels")]
        [SerializeField] private GameObject _dailyRewardPanel;
        [SerializeField] private GameObject _mainPanel;
        [SerializeField] private GameObject _settingsPanel;
        [SerializeField] private GameObject _shopPanel;
        [SerializeField] private GameObject _winPanel;
        [SerializeField] private GameObject _losePanel;
        [SerializeField] private GameObject _loadPanel;

        [Header("ToMainPanel")]
        [SerializeField] private GameObject _progressBar;
        [SerializeField] private Text _currentLevelText;
        [SerializeField] private Text _nextLevelText;
        [SerializeField] private Text _scoreText;

        [Header("ToWinPanel")]
        [SerializeField] private Text _winText;
        [SerializeField] private Image[] _winStars;

        [Header("ToLosePanel")]
        [SerializeField] private Text _loseText;
        [SerializeField] private Image[] _loseStars;

        [Header("ToShopPanel")]
        [SerializeField] private GameObject _cameraOnShop;
        [SerializeField] private Light _shopLight;
        [SerializeField] private Skin[] _skins;

        [Header("Oter scripts")]
        [SerializeField] private HeroTransformation _hero;

        [Header("OnStartGame")]
        [SerializeField] private GroundGenerator _groundGenerator;
        [SerializeField] private GameObject _SwipeAndHold;
        [SerializeField] private GameObject _cameraOnRoad;
        [SerializeField] private GameObject _confetty;
        [SerializeField] private GameObject _joystic;
        [SerializeField] private GameObject _player;
        [SerializeField] private GameObject _playerRunTransform;
        [SerializeField] private GameObject _playerLieTransform;
        [SerializeField] private GameObject _bonusPlayer;
        [SerializeField] private GameObject[] money;

        [SerializeField] private GameObject GroundStart;
        [SerializeField] private GameObject GroundEnd;
        [SerializeField] private GameObject Ground;

        [SerializeField] private string[] _questions;

        [SerializeField] private int[] _answers;

        [SerializeField] private float speedSettings;

        [SerializeField] private Text _questionText;

        private bool _firstSession = true;

        private float speed;

        private int _questionNumber;
        private int _currentDoor;
        private int _rightAnswer;
        private int coins;
        private int level = 1;
        private int random;

        private DoorOpen[] _doors;

        private void Start()
        {
            Question();


            int stars = PlayerPrefs.GetInt("Stars");
            stars = 0;
            PlayerPrefs.SetInt("Stars", stars);

            _groundGenerator.ViewStartPanel();

            if (_viewAds == true)
                _toyStreamSdk.Initialize();

            _doors = FindObjectsOfType<DoorOpen>();

            foreach (DoorOpen doors in _doors)
            {
                doors.Entered += Entered;
            }


            _hero.Win += Win; 
            _hero.Lose += Lose;

            level = PlayerPrefs.GetInt("Level");

            random = Random.Range(50, 150);

            if (level == 0)
            {
                PlayerPrefs.SetFloat("Speed", speedSettings);
                _gameAnalitics.FirstSession();
                _firebase.FirstSession();
            }

            //PlayerPrefs.SetFloat("Speed", speedSettings);

            int PlayerScore = PlayerPrefs.GetInt("PlayerScore");
      
            PlayerPrefs.SetInt("PlayerScore", PlayerScore);

            int Score = PlayerPrefs.GetInt("Score");

            //Score = 0; 
            //level = 0;
            //PlayerPrefs.SetInt("Score", Score);
            //PlayerPrefs.SetInt("Level", level);
            _scoreText.text = $"{Score}";
        }

        void Update()
        { 
            Ground.transform.Translate(Vector3.forward * (-1) * Time.deltaTime * speed);
        }

        private void Entered(DoorOpen door)
        {
            _currentDoor = door.id;

            if (_currentDoor != _rightAnswer)
            {
                Lose();
            }
            Invoke(nameof(Question), 0.6f);
        }

        private void Question()
        {
            _questionNumber = Random.Range(0, _questions.Length);
            _rightAnswer = _answers[_questionNumber];
            _questionText.text = $"{_questions[_questionNumber]}";
        }

        private void Win()
        {
            _bonusPlayer.SetActive(false);
            //_playerAnimator.enabled = true;

            OpenStar();
            _questionText.gameObject.SetActive(false);
            _joystic.SetActive(false);

            money[0].GetComponent<ParticleSystem>().Play();
            money[1].GetComponent<ParticleSystem>().Play();

            coins = PlayerPrefs.GetInt("Score");
            coins += random;
            PlayerPrefs.SetInt("Score", coins);
            _scoreText.text = $"{coins}";
                                    
            Invoke(nameof(WinPanel), 5);
        }

        private void OpenStar()
        {
            int stars = PlayerPrefs.GetInt("Stars");
            Debug.Log(stars);
            for (int i = 0; i < stars; i ++)
            {
                _winStars[i].color = new Color(255.0f, 255.0f, 255.0f, 255.0f);
                _loseStars[i].color = new Color(255.0f, 255.0f, 255.0f, 255.0f);
            }
        }

        private void CloseStar()
        {
            int stars = PlayerPrefs.GetInt("Stars");
            
            for (int i = 0; i < 3; i++)
            {
                _winStars[i].color = new Color(0, 0, 0, 0);
                _loseStars[i].color = new Color(0, 0, 0, 0);
            }
        }

        private void WinPanel()
        {
            _winPanel.SetActive(true);

            speed = 0;
            level = PlayerPrefs.GetInt("Level");
            level ++; 
            PlayerPrefs.SetInt("Level", level);
    
            _winText.text = $"Congratulations, you win and get {random} coins!";

            _gameAnalitics.LevelComplete(level);
            _firebase.LevelComplete(level);

            Invoke(nameof(ViewIntersticial), 3);
        }

        private void Lose()
        {
            OpenStar();
            _joystic.SetActive(false);
            _winPanel.SetActive(true);
            _losePanel.SetActive(true);

            speed = 0;

            _loseText.text = $"Oh, you lost, try again!";

            _gameAnalitics.LevelFail(level);
            _firebase.LevelFail(level);

            Invoke(nameof(ViewIntersticial), 3); 
        }

        private void Loading()
        {
            SceneManager.LoadScene(0);
            _loadPanel.SetActive(true);
            //ViewIntersticial();
            //Invoke(nameof(OpenMenu), 2); 
        }

        private void OpenMenu()
        {
            level = PlayerPrefs.GetInt("Level");

            CloseStar();

            _loadPanel.SetActive(false);
            _progressBar.SetActive(false);
            _scoreText.gameObject.SetActive(true);
            _confetty.SetActive(false);
            
            _joystic.SetActive(false);
            _winPanel.SetActive(false);
            _losePanel.SetActive(false);
            _mainPanel.SetActive(true);

        }

        public void ViewIntersticial()
        {
            //SceneManager.LoadScene(0);
            //_loadPanel.SetActive(true);
            //CloseShop();
            _toyStreamSdk.ShowInterstitial("continue", () => Loading());
            Loading();
        }

        public void Close()
        {
            Loading();
            _losePanel.SetActive(false);
            _winPanel.SetActive(false);
        }

        public void OpenShop()
        {
            _bonusPlayer.SetActive(false);

            _progressBar.SetActive(false);
            _cameraOnRoad.SetActive(false);
            _mainPanel.SetActive(false);
            _joystic.SetActive(false);

            _scoreText.gameObject.SetActive(true);
            _cameraOnShop.SetActive(true);
            _shopPanel.SetActive(true);
            _shopLight.enabled = true;
        }
        
        public void CloseShop()
        {
            _cameraOnShop.SetActive(false);
            _shopLight.enabled = false;
            _shopPanel.SetActive(false);

            _scoreText.gameObject.SetActive(true);
            _cameraOnRoad.SetActive(true);
            _mainPanel.SetActive(true);
            _joystic.SetActive(true);

            int Score = PlayerPrefs.GetInt("Score");
            _scoreText.text = coins.ToString();

            Loading();
        }

        public void TapToPlay()
        {
            if (level == 0)
                _SwipeAndHold.SetActive(true);
            else
                _SwipeAndHold.SetActive(false);

            _scoreText.gameObject.SetActive(false);
            _mainPanel.SetActive(false);

            if (level % 5 == 0 && level != 0)
            {
                _bonusPlayer.SetActive(true);
                //_playerAnimator.enabled = false;
                _questionText.gameObject.SetActive(false);
                _player.transform.rotation = _playerLieTransform.transform.rotation;
            }
            else
            {
                _bonusPlayer.SetActive(false);
                _questionText.gameObject.SetActive(true);
                //_playerAnimator.enabled = true;
                _player.transform.rotation = _playerRunTransform.transform.rotation;
            }

            _progressBar.SetActive(true);
            _joystic.SetActive(true);

            _currentLevelText.text = $"{level}";
            _nextLevelText.text = $"{level + 1}";

            speedSettings = PlayerPrefs.GetFloat("Speed");
            speedSettings += 0.02f;
            PlayerPrefs.SetFloat("Speed", speedSettings);

            speed = speedSettings;
            
            _gameAnalitics.LevelStart(level);
            _firebase.LevelStart(level);
        }

        public void GetDayReward()
        {
            _dailyRewardPanel.SetActive(true);
        }

        public void CloseDailyReward()
        {
            _losePanel.SetActive(false);
            _winPanel.SetActive(false);
        }

        private void OnDestroy()
        {
            _hero.Win -= Win;
            _hero.Lose -= Lose;

            foreach (DoorOpen doors in _doors)
            {
                doors.Entered -= Entered;
            }
        }
    }
}
