﻿using UnityEngine;

public class Skin : MonoBehaviour
{
    [SerializeField] public bool _isBought;
    [SerializeField] public bool _isSelected;
    [SerializeField] public float _price;
}
