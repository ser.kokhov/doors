﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CodeBase
{
    public class SkinChanger : MonoBehaviour
    {
        [SerializeField] private Game _game;

        [SerializeField] private Transform _player;
        [SerializeField] private Transform _playerHead;

        [SerializeField] private Skin[] _skins;

        private bool[] _stockCheck;
        public int index;
        private int coins;

        [SerializeField] private Button _selectButton;
        [SerializeField] private Text _priceText;

        [SerializeField] private Text _scoreText;
        [SerializeField] private Button _nextButton;
        [SerializeField] private Button _lastButton;
        [SerializeField] private GameObject Fog;
        [SerializeField] private GameObject Stars;

        void Awake()
        {
            //coins = PlayerPrefs.GetInt("Score");
            //_scoreText.text = coins.ToString();

            index = PlayerPrefs.GetInt("SkinIndex");

            _stockCheck = new bool[20];
            if (PlayerPrefs.HasKey("StockArray"))
                _stockCheck = PlayerPrefsX.GetBoolArray("StockArray");
            else
                _stockCheck[0] = true;

            _skins[index]._isSelected = true;

            for (int i = 0; i < _skins.Length; i++)
            {
                _skins[index]._isBought = _stockCheck[i];
                if (i == index)
                {
                    _player.GetChild(i).gameObject.SetActive(true);
                    _playerHead.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    _player.GetChild(i).gameObject.SetActive(false);
                    _playerHead.GetChild(i).gameObject.SetActive(false);
                }
            }

            _priceText.text = "CHOSEN";
            _selectButton.interactable = false;
        }

        private void Update()
        {
            coins = PlayerPrefs.GetInt("Score");
            _scoreText.text = coins.ToString();
        }

        private void SaveSkin()
        {
            PlayerPrefsX.SetBoolArray("StockArray", _stockCheck);
        }

        public void NextSkin()
        {
            Fog.GetComponent<ParticleSystem>().Play();
            if (index < _player.childCount - 1)
            {
                index++;

                if (_skins[index]._isBought && _skins[index]._isSelected)
                {
                    _priceText.text = "CHOSEN";
                    _selectButton.interactable = false;
                }
                else if (!_skins[index]._isBought)
                {
                    _priceText.text = $"{_skins[index]._price}";
                    _selectButton.interactable = true;
                }
                else if (_skins[index]._isBought && !_skins[index]._isSelected)
                {
                    _priceText.text = "CHOOSE";
                    _selectButton.interactable = true;
                }

                for (int i = 0; i < _player.childCount; i++)
                {
                    _player.GetChild(i).gameObject.SetActive(false);
                    _playerHead.GetChild(i).gameObject.SetActive(false);
                }

                    _player.GetChild(index).gameObject.SetActive(true);
                _playerHead.GetChild(index).gameObject.SetActive(true);
            }
        }

        public void LastSkin()
        {
            Fog.GetComponent<ParticleSystem>().Play();
            if (index > 0)
            {
                index--;
                
                if (_skins[index]._isBought && _skins[index]._isSelected)
                {
                    _priceText.text = "CHOSEN";
                    _selectButton.interactable = false;
                }
                else if (!_skins[index]._isBought)
                {
                    _priceText.text = $"{_skins[index]._price}";
                    _selectButton.interactable = true;
                }
                else if (_skins[index]._isBought && !_skins[index]._isSelected)
                {
                    _priceText.text = "CHOOSE";
                    _selectButton.interactable = true;
                }

                for (int i = 0; i < _player.childCount; i++)
                {
                    _player.GetChild(i).gameObject.SetActive(false);
                    _playerHead.GetChild(i).gameObject.SetActive(false);
                }

                _player.GetChild(index).gameObject.SetActive(true);
                _playerHead.GetChild(index).gameObject.SetActive(true);
            }
        }

        public void BuyButtonAction()
        {
            Stars.GetComponent<ParticleSystem>().Play();
            if (_selectButton.interactable && !_skins[index]._isBought)
            {
                if (coins >= int.Parse(_priceText.text))
                {
                    coins -= int.Parse(_priceText.text);
                    _scoreText.text = coins.ToString();
                    PlayerPrefs.SetInt("Score", coins);
                    _stockCheck[index] = true;
                    _skins[index]._isBought = true;
                    _priceText.text = $"CHOOSE";
                    SaveSkin();
                }
            }
            if (_selectButton.interactable && _skins[index]._isBought && !_skins[index]._isSelected)
            {
                PlayerPrefs.SetInt("SkinIndex", index);
                _selectButton.interactable = false;
                _priceText.text = "CHOSEN";
                Invoke(nameof(Interstitial), 1.5f);
            }
        }

        private void Interstitial()
        {
            _game.ViewIntersticial();
        }
    }
}
