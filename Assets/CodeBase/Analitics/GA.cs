using UnityEngine;
using GameAnalyticsSDK;

public class GA : MonoBehaviour
{

    void Start()
    {
        GameAnalytics.Initialize();
    }

    public void FirstSession()
    {
        GameAnalytics.NewDesignEvent( $"First start");
    }
    public void GameStart()
    {
        GameAnalytics.NewDesignEvent($"Start game");
    }
    public void LevelStart(int level)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, $"Level_{level}_start");
    }
    public void LevelComplete(int level)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, $"Level_{level}_completed");
    }
    public void LevelFail(int level)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, $"Level_{level}_failed");
    }
    public void BannerStart()
    {
        GameAnalytics.NewDesignEvent("Banner_start");
    }
    public void BannerClosed()
    {
        GameAnalytics.NewDesignEvent("Baner_closed");
    }
    public void InterStatrt()
    {
        GameAnalytics.NewDesignEvent("InterStart");
    }
    public void InterShown()
    {
        GameAnalytics.NewDesignEvent("InterShown");
    }
    public void InterFailed()
    {
        GameAnalytics.NewDesignEvent("InterFailed");
    }
}
