﻿using UnityEngine;
using GameAnalyticsSDK;
using System;

namespace CodeBase
{
    public class ToyStreamSDK : MonoBehaviour
    {
        [SerializeField] private ApplovinToyStream _applovin;

        public void Initialize()
        {
            _applovin.Initialize();
            
            GameAnalytics.Initialize();
            GameAnalytics.NewDesignEvent("game_start");

            CheckFirstSession();

            Application.logMessageReceived += HandleLog;
        }

        private void OnDestroy()
        {
            Application.logMessageReceived -= HandleLog;
        }

        public bool CanShowInterstitial { get; }

        public void ShowInterstitial(string placement, Action callback)
        {
            _applovin.ShowInterstitial(placement, () =>
            {
                callback.Invoke();

                GameAnalytics.NewAdEvent(
                    GAAdAction.Show,
                    GAAdType.RewardedVideo,
                    _applovin.SdkName,
                    placement);
            });
        }

        public void ShowRewarded(string placement, bool canShowInterstitial, Action<bool> callback)
        {
            //CanShowInterstitial = canShowInterstitial;

            if (/*Debug.isDebugBuild && */false)
                callback.Invoke(true);
            else
                _applovin.ShowRewarded(placement, (finishRewarded) =>
                {
                    callback.Invoke(finishRewarded);

                    GameAnalytics.NewAdEvent(
                        finishRewarded ? GAAdAction.RewardReceived : GAAdAction.FailedShow,
                        GAAdType.RewardedVideo,
                        _applovin.SdkName,
                        placement);
                });
        }

        public void OnStartLevel(int level) =>
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, level.ToString());

        public void OnCompleteLevel(int level) =>
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, level.ToString());

        public void OnFailLevel(int level) =>
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, level.ToString());

        private void HandleLog(string logString, string stackTrace, LogType type)
        {
            if (type == LogType.Error || type == LogType.Exception)
                GameAnalytics.NewErrorEvent(GAErrorSeverity.Error, logString + '\n' + stackTrace);
        }

        private void CheckFirstSession()
        {
            if (PlayerPrefs.GetInt("first_session", -1) == -1)
            {
                PlayerPrefs.SetInt("first_session", 1);
                GameAnalytics.NewDesignEvent("first_session");
            }
        }
    }
}